#BUILDING

dependencies: MPI implementation for CRAY, cuda-6.5+
cd bc2d/generator
make -f Makefile.CSCS.mpi
cd ../src
make -f Makefile.cray


EXEC: bc2d/bin



./bc2d-1d -h
Usage:
         $> bin/bc2d-1d -p RxC  [-o outfile] [-D] [-d] [-m] [-N <# of serarch>] [-H 0,1]

         -> to visit a graph read from file:
                 -f <graph file> -n <# vertices> [-r <start vert>]
         -> to visit an RMAT graph:
                 -S <scale> [-E <edge factor>]
         Where:
                 -RxC  grid of processors
                 -D to ENABLE debug information
                 -d to DUMP RMAT generated graph to file
                 -m to DISABLE mono GPU optimization
                 -o file_scores_bc
                 -H 1-degree reduction off (0)

#######################################
R-MAT EXAMPLE:
graph scale 20
graph ef 16
1-degree on
4 processors arranged in 2x2 mesh
bc rounds 10000

$> mpirun -np 4 bc2d-1d -p 2x2 -S 20 -E 16 -N 10000 -H 1


R-MAT EXAMPLE
graph scale 20
graph ef 16
1-degree ON
processors 8 arranged in 2 subclusters 2x2.
bc rounds 10000 BC round.

$> mpirun -np 8 bc2d-1d -p 2x2 -S 20 -E 16 -N 10000 -H 1

